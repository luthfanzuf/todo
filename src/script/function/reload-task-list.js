import { formatDate } from "./date.js"
import { deleteTask, editTask } from "./task-delete-edit.js"
import { saveToLocalStorage, updateArrFromLocalStorage } from "../function/sync.js"

function clearList(listWrapperElement) {
    listWrapperElement.innerHTML = '';
    updateArrFromLocalStorage();  
}

function loadList(tasklistArray) {

    let arrayWithValuesOnly = tasklistArray.map(Object.values)

    arrayWithValuesOnly.forEach(function(value) {
        var element  = document.querySelector("task-list").shadowRoot.getElementById("list-wrapper").appendChild(document.createElement("task-item")); //element.className = "task-item";
        var fragment = document.createDocumentFragment();

        
        var divDate = document.createElement('div'); divDate.className = "date";
        var divTask = document.createElement('div'); divTask.className = "task";
        var divEdit = document.createElement('edit'); divEdit.className = "edit-btn"; divEdit.setAttribute("id", value[3]);
        var divDelete = document.createElement('delete'); divDelete.className = "del-btn"; divDelete.setAttribute("id", value[3]);
        var divCancel = document.createElement('cancel'); divCancel.className = "cancel-btn"; divCancel.setAttribute("id", value[3]); divCancel.setAttribute("style", "display: none"); 
        var divSaveEdit = document.createElement('save'); divSaveEdit.className = "save-btn"; divSaveEdit.setAttribute("id", value[3]); divSaveEdit.setAttribute("style", "display: none");

        var divCheck = document.createElement('div'); divCheck.className = "check-btn";
        var stringContainingXMLSource = '<svg height="0px" width="0px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 341.333 341.333"><path d="M170.667 0C76.41 0 0 76.41 0 170.667s76.41 170.667 170.667 170.667 170.667-76.41 170.667-170.667S264.923 0 170.667 0zm0 298.667c-70.692 0-128-57.308-128-128s57.308-128 128-128 128 57.308 128 128-57.308 128-128 128z"/></svg>'

        divCheck.innerHTML = stringContainingXMLSource
        divDate.textContent = formatDate(value[2]);
        divTask.textContent = value[0];
        divEdit.textContent = 'Edit';
        divDelete.textContent = 'Delete';
        divCancel.textContent = 'Cancel';
        divSaveEdit.textContent = 'Save';

        //fragment.appendChild(divDate);
        fragment.appendChild(divCheck);
        fragment.appendChild(divTask);
        fragment.appendChild(divEdit);
        fragment.appendChild(divDelete);
        fragment.appendChild(divCancel);
        fragment.appendChild(divSaveEdit);

        element.appendChild(fragment);
    });
    

}

export function reloadList(tasklistArray, listWrapperElement){
    updateArrFromLocalStorage();
    clearList(listWrapperElement);
    loadList(tasklistArray);
    listenCrudEvent();

    function listenCrudEvent() {
        let delSelect = document.querySelector("task-list").shadowRoot.querySelectorAll('.del-btn');
        for (var i = 0 ; i < delSelect.length; i++) {
            delSelect[i].addEventListener('click', clickDelete, false );
        };
    
        let editSelect = document.querySelector("task-list").shadowRoot.querySelectorAll('.edit-btn');
        for (var j = 0 ; j < editSelect.length; j++) {
            editSelect[j].addEventListener('click', clickEdit, false );
        };
    
        let cancelSelect = document.querySelector("task-list").shadowRoot.querySelectorAll('.cancel-btn');
        for (var k = 0 ; k < cancelSelect.length; k++) {
            cancelSelect[k].addEventListener('click', clickCancel, false );
        };

        let saveSelect = document.querySelector("task-list").shadowRoot.querySelectorAll('.save-btn');
        for (var l = 0 ; l < saveSelect.length; l++) {
            saveSelect[l].addEventListener('click', clickSave, false );
        };

        let editableSelect = document.querySelector("task-list").shadowRoot.querySelectorAll('.editable');

        for (var m = 0 ; m < editableSelect.length; m++) {
            editableSelect[m].addEventListener('keydown', (evt) => {
                if (evt.key === "Enter") {
                    evt.preventDefault();
                    document.execCommand('insertHTML', false, '<br/>');
                    console.log("enter has pressed!");
                    return false;   
                }
            });
        };
    }

    //redefine delete button everytime list reloaded
    function clickDelete(event){
        // do something here
        event.preventDefault();

        //get index
        var delIndex = tasklistArray.findIndex(i => i.id == this.getAttribute("id"));

        //send log to console
        console.log("you pressing delete button! with indexref: " + delIndex);

        //delete stuff
        deleteTask(delIndex, tasklistArray);

        //log array after delete
        console.log(tasklistArray);

        //reload html
        clearList(listWrapperElement);
        loadList(tasklistArray);

        //listen for next crud event
        listenCrudEvent();
    };

    function clickEdit(event){
        // do something here
        event.preventDefault();

        //get index
        var editIndex = tasklistArray.findIndex(j => j.id == this.getAttribute("id"));

        //send log to console
        console.log("you pressing edit button! with indexref: " + editIndex);

        //select and manipulate html
        this.setAttribute("style", "display: none");
        this.nextSibling.setAttribute("style", "display: none");
        this.nextSibling.nextSibling.setAttribute("style", "display: inline-block");
        this.nextSibling.nextSibling.nextSibling.setAttribute("style", "display: inline-block");
        this.previousSibling.setAttribute("contenteditable", "true");
        this.previousSibling.className += " editable";

        //listen for next crud event
        listenCrudEvent();
    };

    function clickCancel(event){
        //do something
        event.preventDefault();

        //get index
        var cancelIndex = tasklistArray.findIndex(i => i.id == this.getAttribute("id"));

        //send log to console
        console.log("you pressing cancel button! with indexref: " + cancelIndex);

        //reload html
        clearList(listWrapperElement);
        loadList(tasklistArray);
        
        //listen for next crud event
        listenCrudEvent();
    };
    function clickSave(event){
        //do something
        event.preventDefault();

        //get index
        var saveIndex = tasklistArray.findIndex(i => i.id == this.getAttribute("id"));

        //send log to console
        console.log("you pressing save button! with indexref: " + saveIndex);

        var inputWithBr = this.previousSibling.previousSibling.previousSibling.previousSibling.innerHTML;

        var titleInput = inputWithBr.replace(/(&nbsp;|<br>|<br \/>)/gm, '');

        //save then reload html
        async function saveThenReloadThenListen(){
            await editTask(saveIndex, titleInput, tasklistArray); //this function handle splice to array

            //save to local storage
            await saveToLocalStorage(); 

            //this function relaod html
            clearList(listWrapperElement);
            loadList(tasklistArray);

            //listen for next crud event
            listenCrudEvent();
        }
        saveThenReloadThenListen();
    };
};






