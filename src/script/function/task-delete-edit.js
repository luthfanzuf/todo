import { saveToLocalStorage, updateArrFromLocalStorage } from "../function/sync.js"

export function deleteTask(delIndex, tasklistArray) {
    tasklistArray.splice(delIndex,1);
    saveToLocalStorage () 
     
};

export function editTask(saveIndex, titleInput, tasklistArray) {
    
    //para as selector
    //const titleInput = document.querySelector("new-task").shadowRoot.getElementById("task-title").value;

    new Promise ((newObj)=> {
        const editTask = new Object()

        editTask.title = titleInput;
        editTask.description = "";
        editTask.duedate = "";
        editTask.id = saveIndex;
        return newObj(editTask)

    }).then(validateInput=> {

        if (titleInput.length === 0) {
            return alert("title can't be empty");
        };

        return tasklistArray.splice(saveIndex, 1, validateInput);
    }).then(x=> {
        
        console.log(tasklistArray);        
    })
};