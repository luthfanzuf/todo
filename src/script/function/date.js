export function formatDate(dateString){
    const options = { weekday: 'long', hour: '2-digit', minute: '2-digit', hour12: true }
    if(dateString == "") {
        return ""
    } else {
        return new Date(dateString).toLocaleDateString('id-ID', options)
    }
};