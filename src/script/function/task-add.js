import { reloadList } from "./reload-task-list.js"
import { saveToLocalStorage, updateArrFromLocalStorage } from "../function/sync.js"

export function addTask(taskList) {
    new Promise ((inputObject) => {
        //get input value
        const duedateInput = document.querySelector("new-task").shadowRoot.getElementById("task-duedate").value;
        const titleInput = document.querySelector("new-task").shadowRoot.getElementById("task-title").value;        
        const descriptionInput = document.querySelector("new-task").shadowRoot.getElementById("task-description").value;
    
        //validate input
        if (titleInput.length === 0) {
            return alert("title can't be empty");
        }
    
        //create input object
        const newTask = new Object()
        newTask.title = titleInput;
        newTask.description = descriptionInput;
        newTask.duedate = duedateInput;
        newTask.id = taskList.length;
        return inputObject(newTask)
    
    }).then(newTask => {
    
        //push to tasklist
        taskList.push(newTask)
        console.log(taskList)
        console.log("new task successfuly added to tasklist")
        console.log("your tasklist length right now is " + taskList.length)
    
    }).then(x => {

        //selectListElement
        const selectListElement = document.querySelector("task-list").shadowRoot.getElementById("list-wrapper");

        //sync tasklist dan localstorage
        saveToLocalStorage();

        //see reload-task-list.js
        reloadList(taskList, selectListElement);
        console.log(taskList);            

    }).then(z => {

        //empty input field
        document.querySelector("new-task").shadowRoot.getElementById("task-duedate").value = ""
        document.querySelector("new-task").shadowRoot.getElementById("task-title").value = ""      
        document.querySelector("new-task").shadowRoot.getElementById("task-description").value = ""
    
        //close new task menu
        document.querySelector("new-task").shadowRoot.getElementById("new-menu").style.display = "none";
        document.getElementsByClassName("modal")[0].style.display = "none";    
        
    })
}