// MASIH WIP (work in progress ^^)
//fungsi untuk load data ketika pertama kali akses

import { tasklist } from "../data/data.js"
import { selectListElement } from "../component/task-list"
import { reloadList } from "./reload-task-list"
import { updateArrFromLocalStorage } from "./sync.js"

export async function pageLoad() {

    if (localStorage.getItem("tasklist") === null) {
        reloadList( tasklist, selectListElement);
    } else {
        await updateArrFromLocalStorage();
        reloadList( tasklist, selectListElement);
    }
}