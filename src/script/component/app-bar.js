class AppBar extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
        const root = this.shadowRoot;        
    }
    connectedCallback(){
        this.render();
    }
    
    render(){
        this.shadowDOM.innerHTML = `
        <style>
            * {
                margin: 0;
                padding : 0;
                box-sizing: border-box
            }
            .centered-xy {
                position: fixed;
                top: 50%;
                left: 50%;
                /* bring your own prefixes */
                transform: translate(-50%, -50%);
              }

            .container {
                width: 100%;
                background-color: darkslategrey;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                text-align: center;
                height: 60px;
                line-height: 60px;
            }
            
            #open-toggle {
                display:inline-block;
                color: white;
                position: absolute;
                left: 10px;
                top: 3px;
            }

            #menu-name {
                display:inline-block;
                color: white
            }
            
            /* sidebar start here */

            .sidebar {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 5;
                top: 0;
                left: 0;
                background-color: #111;
                overflow-x: hidden;
                transition: 0.5s;
                padding-top: 60px;
              }
              
              .sidebar a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: #818181;
                display: block;
                transition: 0.3s;
                
              }
              
              .sidebar a:hover {
                color: #f1f1f1;
              }
              
              .sidebar .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
                cursor: pointer;
              }
              
              .openbtn {
                font-size: 20px;
                cursor: pointer;
                background-color: darkslategrey;
                color: white;
                padding: 10px 15px;
                border: none;
              }
              
              .openbtn:hover {
                background-color: black;
              }

              #about-modal, #feedback-modal {
                float: middle;
                position: fixed;
                margin-top: 10px;
                padding: 25px;
                display: none;
                width: 600px;
                background-color: darkslategrey;
                text-align: left;
                color: white;
                z-index: 3;
                border-radius: 25px;
            }
            svg {
                max-height: 30px;
                max-width: 30px;
                display: inline-block;
            }

            h2 {
                display: inline-block;
                margin: 0 0 6px 0;
            }

            ul {
                list-style: disc outside none;
                padding: 10px 10px 10px 25px;
            }

            li {
                padding: 2px;
            }

            p {
                padding: 10px;
            }

            #about-close, #feedback-close {
                right: 15px;
                top: 15px;
                position: absolute;
                cursor: pointer;              
            }

            @media only screen and (max-width: 610px) {
                #about-modal, #feedback-modal {
                    width: 97%;
                }   
            }

        </style>
        <div id="nav-bar" class="container">
            <div id="open-toggle">
                <button class="openbtn" id="open-toggle">☰</button>
            </div>
            <div id="menu-name">
                <h2>To Do</h2>
            </div>
        </div>
        <div id="mySidebar" class="sidebar">
            <a class="closebtn" id="close-toggle">&times;</a>
            <a id="open-about" href="#">About</a>
            <a id="open-feedback" href="#">Feedback</a>
        </div>
        <div id="about-modal" class="centered-xy">
            <h2 id="about-menu-header">About This Website</h2>
            <svg id="about-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
                <line x1="15" y1="15" x2="25" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>
                <line x1="25" y1="15" x2="15" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>    
                <circle class="circle" cx="20" cy="20" r="19" opacity="0" stroke="#000" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></circle>
                <path d="M20 1c10.45 0 19 8.55 19 19s-8.55 19-19 19-19-8.55-19-19 8.55-19 19-19z" class="progress" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></path>
            </svg>
            <div id="about-menu-content">
            <p>This website is simple to-do app made by twitter @luthfanzuf.
            Features in this app (for now):</p>
            <ul>
                <li>simple to-do crud apps</li>
                <li>ability to save your to-do list in browser localstorage</li>
            </ul>
            <p>Note: make sure to not clear cache of this website, if you want to save your to-do list.</p>
            </div>
        </div>
        <div id="feedback-modal" class="centered-xy">
            <h2 id="feedback-menu-header">Feedback</h2>
            <svg id="feedback-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
                <line x1="15" y1="15" x2="25" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>
                <line x1="25" y1="15" x2="15" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>    
                <circle class="circle" cx="20" cy="20" r="19" opacity="0" stroke="#000" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></circle>
                <path d="M20 1c10.45 0 19 8.55 19 19s-8.55 19-19 19-19-8.55-19-19 8.55-19 19-19z" class="progress" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></path>
            </svg>
            <div id="about-menu-content">Please send your feedback to twitter @luthfanzuf (for now).
            </div>
        </div>

        <script>
        `;
        
        const menuIcon = this.shadowRoot.getElementById('open-toggle');
        menuIcon.addEventListener('click', this.openMenu);
        const closeIcon = this.shadowRoot.getElementById('close-toggle');
        closeIcon.addEventListener('click', this.closeMenu);

        const openAbout = this.shadowRoot.getElementById('open-about');
        const openFeedback = this.shadowRoot.getElementById('open-feedback');
        const closeAbout = this.shadowRoot.getElementById('about-close');
        const closeFeedback = this.shadowRoot.getElementById('feedback-close');

        openAbout.addEventListener('click', this.openAbout);
        openFeedback.addEventListener('click', this.openFeedback);

        closeAbout.addEventListener('click', this.closeAbout);
        closeFeedback.addEventListener('click', this.closeFeedback);
        
    }
    openMenu() {
        document.querySelector("app-bar").shadowRoot.getElementById("mySidebar").style.width = "250px";
        console.log('sidebar opened!');
        }
    closeMenu() {
        document.querySelector("app-bar").shadowRoot.getElementById("mySidebar").style.width = "0";
        console.log('sidebar closed!');
            }
    openAbout() {
        document.querySelector("app-bar").shadowRoot.getElementById("about-modal").style.display = "block";
        document.getElementsByClassName("modal")[0].style.display = "block";
        document.querySelector("app-bar").shadowRoot.getElementById("mySidebar").style.width = "0";    
        console.log('about menu opened!');
        }
    closeAbout() {
        document.querySelector("app-bar").shadowRoot.getElementById("about-modal").style.display = "none";
        document.getElementsByClassName("modal")[0].style.display = "none";  
        console.log('about menu closed!');
        }
    openFeedback() {
        document.querySelector("app-bar").shadowRoot.getElementById("feedback-modal").style.display = "block";
        document.getElementsByClassName("modal")[0].style.display = "block";
        document.querySelector("app-bar").shadowRoot.getElementById("mySidebar").style.width = "0";    
        console.log('feedback menu opened!');
        }
    closeFeedback() {
        document.querySelector("app-bar").shadowRoot.getElementById("feedback-modal").style.display = "none";
        document.getElementsByClassName("modal")[0].style.display = "none";  
        console.log('feedback menu closed!');
        }
}

customElements.define("app-bar", AppBar);
