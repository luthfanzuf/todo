import { tasklist } from "../data/data.js"
import { reloadList } from "../function/reload-task-list"

// membuat list task berdasarkan array of object
class TaskList extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
        
    }
    connectedCallback(){
        this.render();
        
    }
    render(){

        this.shadowDOM.innerHTML = `
        <style>
            * {
                margin: 0;
                padding : 0;
                box-sizing: border-box
            }

            svg {
                width: 25px;
                height: 25px;
                margin: auto;
            }
            .edit-btn {
                color: blue;
                min-width: 35px;
            }
            .del-btn {
                color:red;
                min-width: 55px;
            }

            .cancel-btn {
                color:purple;
                min-width: 55px;
            }

            .save-btn {
                color:green;
                min-width: 35px;
            }

            .task {
                width: 85%;
                display: inline-block;
                border: 1px solid black;
                margin: 4px 4px 4px 0px;
                padding: 8px;
            }

            task-item {
                width: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            .edit-btn, .del-btn, .cancel-btn, .save-btn {
                display: inline-block;
                border: 1px solid black;
                margin: 4px 4px 4px 0px;
                padding: 8px;
                font-weight: bold;
            }

            .check-btn {
                width: 36px;
                height: 36px;
                display: inline-block;
                border: 1px solid black;
                border-width: 1px 0px 1px 1px;
                margin: 0 0;
                padding: 2px;
                display: flex;
                align-items: center;
            }

            #list-wrapper {
                width: 90%;
                margin-left: auto;
                margin-right: auto;
                margin-top: 20px;
                display: block;
            }

            div{
                display: inline-block;
            } 

            @media only screen and (max-width: 1000px) {
                .task{
                    width: 80%;
                }

                #list-wrapper {
                    width: 90%;
                    margin-left: auto;
                    margin-right: auto;
                    margin-top: 20px;
                    display: block;
                }
            }

            @media only screen and (max-width: 700px) {
                .task{
                    width: 75%;
                }

                #list-wrapper {
                    width: 95%;
                    margin-left: auto;
                    margin-right: auto;
                    margin-top: 20px;
                    display: block;
                }            
            }

            @media only screen and (max-width: 450px) {
                .task, .edit-btn, .del-btn, .cancel-btn, .save-btn {
                    display: inline-block;
                    border: 1px solid black;
                    margin: 2px 2px 2px 0px;
                    padding: 8px;
                    font-size: 12px;
                }

                #list-wrapper {
                    width: 98%;
                    margin-left: auto;
                    margin-right: auto;
                    margin-top: 20px;
                    display: block;
                }
                .check-btn {
                    height: 32px;
                    width: 32px;
                }
                svg {
                    width: 18px;
                    height: 18px;
                    margin: auto;
                }

            }



        </style>
        <wrapper id="list-wrapper" part="box"></wrapper>
        `;
        
    };
    
};
customElements.define("task-list", TaskList);

export const selectListElement = document.querySelector("task-list").shadowRoot.getElementById("list-wrapper");


