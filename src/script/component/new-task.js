// membuat task baru
import { tasklist } from "../data/data.js"
import { addTask } from "../function/task-add.js"

class NewTask extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
        const root = this.shadowRoot;        
    }
    connectedCallback(){
        this.render();
    }
    
    render(){
        this.shadowDOM.innerHTML = `
        <style>
            * {
                margin: 0;
                padding : 0;
                box-sizing: border-box
            }
            .button-container {
                position: fixed;
                background-color: darkslategrey;
                color: white;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                text-align: center;
                max-width: 200px;
                width: 18%;
                height: 45px;
                line-height: 45px;
                border-radius: 50px;
                cursor: pointer;
                right: 40px;
                top: 87%;
            }

            #wrapper {
                position: relative;
                margin: 0;
            }
            #new-menu {
                position: fixed;
                margin-top: 10px;
                padding: 15px;
                display: none;
                min-width: 600px;
                background-color: darkslategrey;
                text-align: center;
                color: white;
                z-index: 2;
            }
            div #new-submit {
                position: relative;
                background-color: #1ad2d2;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                max-width: 200px;
                width: 25%;
                height: 45px;
                line-height: 45px;
                border-radius: 50px;
                cursor: pointer;
            }

            .centered-xy {
                position: fixed;
                top: 50%;
                left: 50%;
                /* bring your own prefixes */
                transform: translate(-50%, -50%);
              }
            
            .centered-x {
                position: absolute;
                left: 50%;
                transform: translate(-50%);
            }

            .ma {
                margin: auto;
            }

            svg {
                max-height: 30px;
                max-width: 30px;
            }

            #new-menu-header {
                margin-top: 11px;
            }
            
            #new-menu-title {
                display: inline-block;
            }

            #new-close {
                right: 15px;
                position: absolute;
                cursor: pointer;
            }

            form {
                margin-top: 15px;
                text-align: left;
                padding: 20px;
            }

            input, textarea {
                margin-bottom: 5px;
                width: 100% !important;
                min-height: 35px;
                text-align: center;
                padding: 5px 20px;
            }

            textarea {
                height: fit-content;
                min-height: 65px;
                max-height: 170px;
                width: 100% !important;
            }

            label {
                font-size: 16px;
                font-family: sans-serif;
            }

            

            h2 {
                font-family: sans-serif;
            
            }

            #modal-new {
                position: fixed;
                width: 100%;
                height: 100vh;
                z-index: 1;
                background-color: black;
                opacity: 0.5;
                display: none;
            }

            #submit-btn {
                padding: 5px;
                width: 100px;
                height: 30px;
                background-color: whitesmoke;
            }

            @media only screen and (max-width: 850px) {
                #new-menu-header {
                    margin-top: 7px;
                }
                label {
                    font-size: 12px;
                    font-family: sans-serif;
                }
                svg {
                    max-height: 24px;
                    max-width: 24px;
                }
                .button-container {
                    width: 130px;
                }
                #new-menu {
                    min-width: 600px;
                }
            }

            @media only screen and (max-width: 650px) {
                #new-menu {
                    min-width: 95%;
                }
                h2 {
                    font-size: 0.9rem;
                }
                form {
                    margin-top: 5px;
                    padding: 15px 0px;
                }

            }

            @media only screen and (max-width: 450px) {
                #new-menu {
                    min-width: 95%;
                }
                svg {
                    bottom: 90%;
                }
                #new-menu-title {
                    margin-top: 12px;
                }
                form {
                    margin-top: 0px;
                    padding: 5px 0px 15px 0px;
                }

            }


        </style>
        <div id="modal-new"> </div>
        <div id="new-menu" class="centered-xy">
            <div id="new-menu-header">
            <h2 id="new-menu-title"> Write Below Things Need To Be Done </h2>
                <svg id="new-close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
                <line x1="15" y1="15" x2="25" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>
                <line x1="25" y1="15" x2="15" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>    
                <circle class="circle" cx="20" cy="20" r="19" opacity="0" stroke="#000" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></circle>
                <path d="M20 1c10.45 0 19 8.55 19 19s-8.55 19-19 19-19-8.55-19-19 8.55-19 19-19z" class="progress" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></path>
                </svg>
            </div>
            <form id="form">
                <label for="task-title">Task Title (required):</label><br>
                <input id="task-title" type="text" name="title" required ></input><br>

                <label for="task-description">Task Description (optional):</label><br>
                <textarea id="task-description" type="text" name="description"></textarea><br>
                
                <label for="task-duedate">Task Due Date (optional):</label><br>
                <input id="task-duedate" type="datetime-local" name="duedate"></input><br>
                
            </form>
            <button id="submit-btn" type="submit"/>Add Task</button>
        </div>
        <div id="wrapper">
            <div id="new-open" class="button-container">+ Add New Task</div>
        </div>
        
        `;
        const newTaskOpen = this.shadowRoot.getElementById('new-open');
        newTaskOpen.addEventListener('click', this.openNewMenu);
        const newTaskClose = this.shadowRoot.getElementById('new-close');
        newTaskClose.addEventListener('click', this.closeNewMenu);
        const formSubmit = this.shadowRoot.getElementById('form');
        formSubmit.addEventListener('submit', submitNewTask);
        this.shadowRoot.getElementById('submit-btn').addEventListener('click', submitNewTask);
    }
    openNewMenu() {
        document.querySelector("new-task").shadowRoot.getElementById("new-menu").style.display = "block";
        document.getElementsByClassName("modal")[0].style.display = "block";    
        console.log('new task menu opened!');
        }
    closeNewMenu() {
        document.querySelector("new-task").shadowRoot.getElementById("new-menu").style.display = "none";
        document.getElementsByClassName("modal")[0].style.display = "none";  
        console.log('new task menu closed!');
        }
}

customElements.define("new-task", NewTask);

function submitNewTask(event) {
    event.preventDefault();
    addTask(tasklist);
};